const express = require('express');
const mysql = require('mysql');
const products = require('./app/products');
const app = express();
const cors = require('cors');

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'tests'
});

connection.connect((err) => {
if(err) {
    console.log(err)
}
    app.use('/', products(connection));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });


});