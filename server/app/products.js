const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = (db) => {

    router.get('/:resource/', (req, res) => {
        db.query(`select * from ${req.params.resource}`, function (error, results) {
            if (error) {
                res.send({error: 'No such table!'});
            } else {
                if (req.params.resource === 'products') {
                    {
                        let answer = results.map(item => {
                            return {
                                id: item.id,
                                name: item.name,
                                category_id: item.category_id,
                                location_id: item.location_id
                            }
                        });
                        console.log(answer);
                        res.send(answer);
                    }
                } else {
                    let answer = results.map(item => {
                        return {id: item.id, name: item.name}
                    });
                    console.log(answer);
                    res.send(answer);
                }
            }
        });
    });

    router.get('/:resource/:id', (req, res) => {
        db.query(`select * from ${req.params.resource} where id = ${req.params.id}`, function (error, results) {
            if (error) {
                res.send({error: 'No such products!'});
            } else {
                console.log(results);
                res.send(results);
            }
        });
    });

    router.post('/:resource/', upload.single('image'), (req, res) => {
        const product = req.body;

        let reqq = '';

        if (req.params.resource === 'products') {

            if (req.file) {
                product.image = req.file.filename;
            } else {
                product.image = null;
            }
            if (product.category_id ===''||product.category_id ===undefined) {
                console.log('fields category_id or location_id has incorrect id');
                // res.send({error: 'fields category_id or location_id has incorrect id'});
            } else {
            reqq = `insert INTO ${req.params.resource} (
                    name, 
                    category_id,
                    location_id,
                    description,
                    datetime,
                    image)
                    VALUES ('${product.name}', '${product.category_id}', '${product.location_id}', '${product.description}', NOW(), '${product.image}')`;
            }
        } else {
            reqq = `insert INTO ${req.params.resource} (
                    name, 
                    description)
                    VALUES ('${product.name}', '${product.description}')`;
        }

        db.query(reqq, function (error, results) {
            if (error) {
                console.log(error);
                res.send({error: 'No such resource!'});
            } else {
                db.query(`select * from ${req.params.resource} where id = ${results.insertId}`, function (error, results) {
                    if (error) {
                        res.send({error: 'No such products!'});
                    } else {
                        console.log(results);
                        res.send(results);
                    }
                });
            }
        });
    });

    router.delete('/:resource/:id', (req, res) => {

        db.query(`delete from ${req.params.resource} where id = ${req.params.id}`, function (error, results) {
            if (error) {
                console.log(error.sqlMessage);
                res.send(error);
            } else {
                console.log('was removed',results);
                res.send('was removed',results);
            }
        });
    });
    router.put('/:resource/:id', (req, res) => {

        const product = req.body;
        let reqq = '';

        if (req.params.resource === 'products') {
            reqq = `UPDATE ${req.params.resource} SET name = '${product.name}', category_id = '${product.category_id}', location_id = '${product.location_id}', description = '${product.description}', datetime = NOW(), image = '${product.image}' where id = ${req.params.id}`;
        } else {
            reqq = `UPDATE ${req.params.resource} SET name = '${product.name}', description = '${product.description}' where id = ${req.params.id}`;
        }
        console.log(reqq);

        db.query(reqq, function (error, results) {
            if (error) {
                console.log(error.sqlMessage);
                res.send(error);
            } else {
                db.query(`select * from ${req.params.resource} where id = ${req.params.id}`, function (error, results) {
                    if (error) {
                        res.send({error: 'No such products!'});
                    } else {
                        console.log(results);
                        res.send(results);
                    }
                });
            }
        });
    });


    return router;
};

module.exports = createRouter;